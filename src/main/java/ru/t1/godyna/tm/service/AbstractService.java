package ru.t1.godyna.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.api.repository.IRepository;
import ru.t1.godyna.tm.api.service.IService;
import ru.t1.godyna.tm.exception.entity.EntityNotFoundException;
import ru.t1.godyna.tm.exception.field.IdEmptyException;
import ru.t1.godyna.tm.exception.field.IndexIncorrectException;
import ru.t1.godyna.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository <M>> implements IService<M> {

    protected final R repository;

    public AbstractService(final R repository) {
        this.repository = repository;
    }

    @Nullable
    public M add(@Nullable final M model) {
        if (model == null) throw new EntityNotFoundException();
        repository.add(model);
        return model;
    }

    @NotNull
    @Override
    public Collection<M> add(@NotNull final Collection<M> models) {
        return repository.add(models);
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull final Collection<M> models) {
        return repository.set(models);
    }

    @NotNull
    public List<M> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Comparator<M> comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @Nullable
    @Override
    public M removeOne(@Nullable final M model) {
        if (model == null) throw new EntityNotFoundException();
        repository.removeOne(model);
        return model;
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final M model = repository.findOneById(id);
        if (model == null) throw new EntityNotFoundException();
        return model;
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        @Nullable final M model = repository.findOneByIndex(index);
        if (model == null) throw new EntityNotFoundException();
        return model;
    }

    @Nullable
    @Override
    public M removeOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.removeOneById(id);
    }

    @Nullable
    @Override
    public M removeOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        return repository.removeOneByIndex(index);
    }

    @Override
    public void removeAll(@Nullable Collection<M> collection) {
        if (collection == null || collection.isEmpty()) return;
        repository.removeAll(collection);
    }

    @NotNull
    @Override
    public Integer getSize() {
        return repository.getSize();
    }

    @Override
    public boolean existsById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsById(id);
    }

    @Override
    public void removeAll() {
        repository.removeAll();
    }

}
